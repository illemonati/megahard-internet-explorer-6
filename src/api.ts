import { ipcRenderer } from "electron";

const api = {
    maximize: () => ipcRenderer.send("maximize"),
    minimize: () => ipcRenderer.send("minimize"),
    close: () => ipcRenderer.send("close"),
    getData: (url: string) => ipcRenderer.invoke("get-data", url),
};

export default api;
