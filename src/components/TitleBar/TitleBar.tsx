import React from "react";

import styles from "./styles.css";
import CloseButton from "../../assets/CloseButton.png";
import MaximizeButton from "../../assets/MaximizeButton.png";
import MinimizeButton from "../../assets/MinimizeButton.png";
import TitleBarTop from "../../assets/TitleBarTop.png";
import LeftHandCorner from "../../assets/LeftHandCorner.png";

enum ButtonAction {
    Maximize,
    Minimize,
    Close,
}

const handleButtonClick = (action: ButtonAction) => {
    switch (action) {
        case ButtonAction.Maximize:
            api.maximize();
            break;
        case ButtonAction.Minimize:
            api.minimize();
            break;
        case ButtonAction.Close:
            api.close();
            break;
    }
};

interface TitleBarProps {
    title?: string;
    favIcon?: string;
}

const TitleBar: React.FC<TitleBarProps> = (props) => {
    const { title, favIcon } = props;
    return (
        <div
            className={styles.TitleBar}
            style={{ background: `url(${TitleBarTop})` }}
        >
            <img src={favIcon || LeftHandCorner} className={styles.FavIcon} />
            <p>{title || "Untitled"} - Megahard Internet Explorer</p>
            <div className={styles.WindowIcons}>
                <img
                    className={styles.WindowButtonIcon}
                    src={MinimizeButton}
                    onClick={() => handleButtonClick(ButtonAction.Minimize)}
                />
                <img
                    className={styles.WindowButtonIcon}
                    src={MaximizeButton}
                    onClick={() => handleButtonClick(ButtonAction.Maximize)}
                />
                <img
                    className={styles.WindowButtonIcon}
                    src={CloseButton}
                    onClick={() => handleButtonClick(ButtonAction.Close)}
                />
            </div>
        </div>
    );
};

export default TitleBar;
