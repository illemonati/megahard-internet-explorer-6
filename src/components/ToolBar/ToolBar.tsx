import React from "react";

import styles from "./styles.css";
import backButtonImage from "../../assets/BackButton.png";
import forwardButtonImage from "../../assets/ForwardButton.png";
import stopLoadingButtonImage from "../../assets/StopLoadingButton.png";
import reloadButtonImage from "../../assets/ReloadButton.png";
import homeImage from "../../assets/Home.png";
import searchImage from "../../assets/Search.png";
import favorites from "../../assets/Favorites.png";
import historyButtonImage from "../../assets/HistoryButton.png";

interface ToolBarProps {
  back?: () => any;
  forward?: () => any;
  reload?: () => any;
  stop?: () => any;
  backEnabled?: boolean;
  forwardEnabled?: boolean;
}

const ToolBar: React.FC<ToolBarProps> = (props) => {
  const { back, forward, backEnabled, forwardEnabled, reload, stop } = props;
  return (
    <div className={styles.ToolBar}>
      <div
        className={backEnabled ? "" : styles.GrayScale}
        onClick={() => back && back()}
      >
        <img src={backButtonImage} />
        <p>Back</p>
      </div>
      <div className={backEnabled ? "" : styles.GrayScale}>
        <p>▾</p>
      </div>
      <div
        className={forwardEnabled ? "" : styles.GrayScale}
        onClick={() => forward && forward()}
      >
        <img src={forwardButtonImage} />
      </div>
      <div className={forwardEnabled ? "" : styles.GrayScale}>
        <p>▾</p>
      </div>
      <div onClick={() => stop && stop()}>
        <img src={stopLoadingButtonImage} />
      </div>
      <div onClick={() => reload && reload()}>
        <img src={reloadButtonImage} />
      </div>
      <div>
        <img src={homeImage} />
      </div>
      <div>
        <div className={styles.VerticalLine} />
      </div>
      <div>
        <img className={styles.SmallerButtonImage} src={searchImage} />
        <p>Search</p>
      </div>
      <div>
        <img className={styles.SmallerButtonImage} src={favorites} />
        <p>Favorites</p>
      </div>
      <div>
        <img src={historyButtonImage} />
      </div>
    </div>
  );
};

export default ToolBar;
