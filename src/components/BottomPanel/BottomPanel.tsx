import React from "react";
import DefaultFavIcon from "../../assets/DefaultFavIcon.png";
import styles from "./styles.css";
import InternetIcon from "../../assets/InternetIcon.png";
import BottomRightIcon from "../../assets/BottomRightIcon.png";

interface BottomPanelProps {
    favIcon?: string;
}

const BottomPanel: React.FC<BottomPanelProps> = (props) => {
    const { favIcon } = props;
    return (
        <div className={styles.BottomPanel}>
            <div className={styles.FavIconContainer}>
                <img
                    src={favIcon || DefaultFavIcon}
                    className={styles.FavIcon}
                />
            </div>
            {[...Array(5).keys()].map((i) => (
                <div key={i} className={styles.EmptyBox} />
            ))}
            <div className={styles.InternetBox}>
                <img src={InternetIcon} className={styles.InternetIcon} />
                <p>Internet</p>
            </div>
            <img src={BottomRightIcon} className={styles.BottomRightIcon} />
        </div>
    );
};

export default BottomPanel;
