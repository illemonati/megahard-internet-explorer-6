import React from "react";

import styles from "./styles.css";

import AddressBar from "../AddressBar/AddressBar";
import MenuBar from "../MenuBar/MenuBar";
import ToolBar from "../ToolBar/ToolBar";

interface TopPanesProps {
  address: string;
  setAddress?: (address: string | ((ad?: string) => string)) => any;
  goToPage?: () => any;
  favIcon?: string;
  back?: () => any;
  forward?: () => any;
  reload?: () => any;
  stop?: () => any;
  backEnabled?: boolean;
  forwardEnabled?: boolean;
}

const TopPanes: React.FC<TopPanesProps> = (props) => {
  const {
    address,
    goToPage,
    forward,
    backEnabled,
    forwardEnabled,
    back,
    favIcon,
    setAddress,
    stop,
    reload,
  } = props;

  return (
    <div className={styles.TopPanes}>
      <div className={styles.MenuBar}>
        <MenuBar />
      </div>
      <div className={styles.ToolBar}>
        <ToolBar
          back={back}
          forward={forward}
          backEnabled={backEnabled}
          forwardEnabled={forwardEnabled}
          reload={reload}
          stop={stop}
        />
      </div>
      <AddressBar
        address={address}
        favIcon={favIcon}
        setAddress={setAddress}
        goToPage={goToPage}
      />
    </div>
  );
};

export default TopPanes;
