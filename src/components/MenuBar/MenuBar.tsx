import React from "react";

import styles from "./styles.css";
import winlogo from "../../assets/winlogo.png";
const menuItems = ["File", "Edit", "View", "Favorites", "Tools", "Help"];

const MenuBar: React.FC = () => {
  return (
    <div className={styles.MenuBar}>
      {menuItems.map((menuItem, i) => (
        <div className={styles.MenuItem} key={i}>
          <p>{menuItem}</p>
        </div>
      ))}
      <div className={styles.MenuWinLogoContainer}>
        <img className={styles.winlogo} src={winlogo} alt="winlogo" />
      </div>
    </div>
  );
};

export default MenuBar;
