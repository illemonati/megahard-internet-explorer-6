import React from "react";

import styles from "./styles.css";

import GoIcon from "../../assets/GoIcon.png";
import DefaultFavIcon from "../../assets/DefaultFavIcon.png";

interface AddressBarProps {
    address: string;
    setAddress?: (address: string | ((ad?: string) => string)) => any;
    goToPage?: () => any;
    favIcon?: string;
}

const AddressBar: React.FC<AddressBarProps> = (props) => {
    const { address, setAddress, goToPage, favIcon } = props;

    const handleAddressChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAddress && setAddress(() => e.target.value);
    };
    const handleAddressKeyUp = (e: React.KeyboardEvent<HTMLInputElement>) =>
        e.key === "Enter" && goToPage && goToPage();

    const handleAddressFocus = (e: React.FocusEvent<HTMLInputElement>) => {
        e.target.select();
    };

    return (
        <div className={styles.AddressBar}>
            <div className={styles.AddressNote}>
                <p>Address</p>
            </div>
            <div className={styles.AddressTextBar}>
                <div className={styles.FavIconContainer}>
                    <img
                        src={favIcon || DefaultFavIcon}
                        className={styles.FavIcon}
                    />
                    <input
                        type="text"
                        className={styles.AddressText}
                        value={address}
                        onChange={handleAddressChange}
                        onKeyUp={handleAddressKeyUp}
                        onFocus={handleAddressFocus}
                    />
                </div>
            </div>
            <div
                className={styles.AddressGo}
                onClick={() => goToPage && goToPage()}
            >
                <img src={GoIcon} className={styles.GoIcon} />
                <p>Go</p>
            </div>
            <div className={styles.AddressLinks}>Links</div>
        </div>
    );
};

export default AddressBar;
