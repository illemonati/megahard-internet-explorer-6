declare namespace StylesCssNamespace {
    export interface IStylesCss {
        AddressBar: string;
        AddressGo: string;
        AddressLinks: string;
        AddressNote: string;
        AddressText: string;
        AddressTextBar: string;
        FavIcon: string;
        FavIconContainer: string;
        GoIcon: string;
    }
}

declare const StylesCssModule: StylesCssNamespace.IStylesCss & {
    /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
    locals: StylesCssNamespace.IStylesCss;
};

export = StylesCssModule;
