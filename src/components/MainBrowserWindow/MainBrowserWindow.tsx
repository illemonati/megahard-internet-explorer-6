import React, { useEffect, useRef, useState } from "react";
import BottomPanel from "../BottomPanel/BottomPanel";
import TitleBar from "../TitleBar/TitleBar";
import TopPanes from "../TopPanes/TopPanes";
import styles from "./styles.css";
import axios from "axios";

const homepage = "https://www.google.com/";

const MainBrowserWindow: React.FC = () => {
  const [address, setAddress] = useState(homepage);
  const [title, setTitle] = useState("Homepage");
  const [favIcon, setFavIcon] = useState("");
  const [realFavIcon, setRealFavIcon] = useState("");
  const [blobFavIcon, setBlobFavIcon] = useState("");
  const [backEnabled, setBackEnabled] = useState(false);
  const [forwardEnabled, setForwardEnabled] = useState(false);
  const webviewRef = useRef<null | HTMLWebViewElement>(null);
  const goToPage = () => {
    console.log(`Go To ${address}`);
    if (!webviewRef.current) {
      console.log("WebView not initialized");
      return;
    }
    const addressClean =
      address.trim().startsWith("https://") ||
      address.trim().startsWith("http://")
        ? address.trim()
        : address.trim() === ""
        ? homepage
        : `http://${address.trim()}`;
    console.log(addressClean);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    webviewRef.current.loadURL(addressClean);
  };

  useEffect(() => {
    if (!webviewRef.current) return;
    webviewRef.current.addEventListener("did-start-navigation", (e: any) => {
      e.isMainFrame && setAddress(() => e.url);
    });
    webviewRef.current.addEventListener("dom-ready", () => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      // webviewRef.current.openDevTools();
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      //   webviewRef.current.insertCSS(`
      //             /* width */
      //             ::-webkit-scrollbar {
      //             width: 0;
      //             }
      //         `);
      setBackEnabled((webviewRef as any).current?.canGoBack());
      setForwardEnabled((webviewRef as any).current?.canGoForward());
    });

    webviewRef.current.addEventListener("page-title-updated", (e: any) => {
      setTitle(() => e.title);
    });

    webviewRef.current.addEventListener("page-favicon-updated", (e: any) => {
      e.favicons && e.favicons[0] && setFavIcon(e.favicons[0]);
    });
  }, [webviewRef.current]);

  useEffect(() => {
    console.log(realFavIcon);
    api.getData(realFavIcon).then((data: Uint8Array) => {
      const blob = new Blob([data]);
      const url = URL.createObjectURL(blob);
      setBlobFavIcon(url);
    });
  }, [realFavIcon]);

  useEffect(() => {
    if (realFavIcon !== favIcon) {
      setRealFavIcon(favIcon);
      console.log(`Set favicon ${favIcon}`);
    }
  }, [favIcon]);

  return (
    <div className={styles.MainBrowserWindow}>
      <TitleBar title={title} favIcon={blobFavIcon} />
      <div className={styles.TopPanesArea}>
        <TopPanes
          address={address}
          setAddress={setAddress}
          goToPage={goToPage}
          favIcon={blobFavIcon}
          back={() => (webviewRef as any).current?.goBack()}
          forward={() => (webviewRef as any).current?.goForward()}
          reload={() => (webviewRef as any).current?.reload()}
          stop={() => (webviewRef as any).current?.stop()}
          backEnabled={backEnabled}
          forwardEnabled={forwardEnabled}
        />
      </div>
      <webview src={homepage} className={styles.WebView} ref={webviewRef} />
      <div className={styles.BottomPanel}>
        <BottomPanel favIcon={blobFavIcon} />
      </div>
    </div>
  );
};

export default MainBrowserWindow;
