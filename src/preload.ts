import { contextBridge } from "electron";
// eslint-disable-next-line import/no-named-as-default
import api from "./api";

contextBridge.exposeInMainWorld("api", api);
