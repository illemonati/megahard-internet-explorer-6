import * as React from "react";
import * as ReactDOM from "react-dom";
import MainBrowserWindow from "./components/MainBrowserWindow/MainBrowserWindow";

function render() {
    ReactDOM.render(<MainBrowserWindow />, document.body);
}

render();
